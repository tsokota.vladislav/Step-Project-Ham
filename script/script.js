/*------------SECTION - OUR SERVICES-------------*/

let servicesTabs = document.querySelectorAll(".services-tabs-title");
let servicesContent = document.querySelectorAll(".hidden");

servicesTabs.forEach((element) => {
  element.addEventListener("click", (event) => {
    servicesTabs.forEach((elem) => {
      elem.classList.remove("active");
    });
    event.target.classList.add("active");
    servicesContent.forEach((elemContent) => {
      if (
        event.target.className === "services-tabs-title active" &&
        event.target.dataset.tab === elemContent.dataset.tab
      ) {
        elemContent.className = "show";
      } else {
        elemContent.className = "hidden";
      }
    });
  });
});

/*------------SECTION - WORK-------------*/

let workTabs = document.querySelectorAll(".work-tabs-title");
let workContent = document.querySelectorAll(".tabs-content");

function initializeImageView() {
  workContent.forEach((elemContent) => {
    elemContent.classList.add("show-img");
    elemContent.classList.remove("hidden-img");
  });
}
initializeImageView();

workTabs.forEach((element) => {
  element.addEventListener("click", (event) => {
    workTabs.forEach((elem) => {
      elem.classList.remove("active-img");
    });
    event.target.classList.add("active-img");

    workContent.forEach((elemContent) => {
      if (
        (event.target.className === "work-tabs-title active-img" &&
          event.target.dataset.name === "All") ||
        event.target.dataset.name === elemContent.dataset.name
      ) {
        elemContent.classList.add("show-img");
        elemContent.classList.remove("hidden-img");
      } else {
        elemContent.classList.add("hidden-img");
        elemContent.classList.remove("show-img");
      }
    });
  });
});

/*------------SECTION - WORK(LOAD-MORE)-------------*/

let btnLoad = document.querySelector(".btn-load");
let workContentMore = document.querySelectorAll(".hidden-img-more");
let loader = document.querySelector(".hidden-loader");

btnLoad.addEventListener("click", function (event) {
  event.target.remove();
  loader.classList.add("lds-dual-ring");
  loader.classList.remove("hidden-loader");
  function activeLoader() {
    workContentMore.forEach((img) => {
      img.classList.remove("hidden-img-more");
      img.classList.add("show-img");
      img.classList.remove("hidden-img");
    });
    let activeTab = document.querySelector(".work-tabs-title.active-img");
    workContent.forEach((elemContent) => {
      if (
        activeTab.dataset.name === "All" ||
        activeTab.dataset.name === elemContent.dataset.name
      ) {
        elemContent.classList.add("show-img");
        elemContent.classList.remove("hidden-img");
      } else {
        elemContent.classList.add("hidden-img");
        elemContent.classList.remove("show-img");
      }
    });
    loader.classList.add("hidden-loader");
  }
  setTimeout(activeLoader, 3000);
});
