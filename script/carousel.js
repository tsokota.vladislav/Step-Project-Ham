/*------------SECTION - OPINIONS-------------*/

$(".slider-for").slick({
  arrows: false,
  fade: true,
});

$(".slider-naw").slick({
  slidesToShow: 4,
  asNavFor: ".slider-for",
  dots: false,
  centerMode: true,
  centerPadding: "0",
  focusOnSelect: true,
  prevArrow: '<button type="button" class="slick-prev-arr"></button>',
  nextArrow: '<button type="button" class="slick-next-arr"></button>',
});
